%define module_name ethercat
%define object1_name ec_master.ko
%define object2_name ec_generic.ko
%define object3_name ec_mini.ko
%define build_kernel 3.10.0-229.7.2.rt56.141.6.el7.centos.x86_64
%define current_kernel %(uname -r)
%define destdir /usr/lib/modules/%{current_kernel}/%{module_name}
%define destdir1 %{destdir}/master
%define destdir2 %{destdir}/devices
%define destdir3 %{destdir}/examples/mini
%define rulesfile 90-ethercat.rules
%define rulesdestdir /etc/udev/rules.d


Summary: %{module_name} kernel module
Name: kmod-%{module_name}
Version: 1.5.2
Release: ESS0
License: GPL
Group: ethercat
BuildRoot: /usr/src/build/temp

%description
%{module_name} kernel module

%prep

%install
mkdir -p %{buildroot}/%{destdir1}
sudo mkdir -p %{destdir1}
mkdir -p %{buildroot}/%{destdir2}
sudo mkdir -p %{destdir2}
mkdir -p %{buildroot}/%{destdir3}
sudo mkdir -p %{destdir3}
mkdir -p %{buildroot}/etc/udev/rules.d/
if [ “%{build_kernel}” != “%{current_kernel}” ]; then
echo “This rpm is for %{build_kernel} kernel version. Ensure that you are using right module/kernel”
exit 1
fi
ls %{destdir1} > /dev/null 2> /dev/null
if [ $? != 0 ]; then
echo “%{destdir1} is not there. Unable to install the driver.”
exit 1
fi
ls %{destdir2} > /dev/null 2> /dev/null
if [ $? != 0 ]; then
echo “%{destdir2} is not there. Unable to install the driver.”
exit 1
fi
ls %{destdir3} > /dev/null 2> /dev/null
if [ $? != 0 ]; then
echo “%{destdir3} is not there. Unable to install the driver.”
exit 1
fi
install -m 755 /home/vagrant/rpmbuild/BUILD/%{object1_name} %{buildroot}%{destdir1}/%{object1_name}
install -m 755 /home/vagrant/rpmbuild/BUILD/%{object2_name} %{buildroot}%{destdir2}/%{object2_name}
install -m 755 /home/vagrant/rpmbuild/BUILD/%{object3_name} %{buildroot}%{destdir3}/%{object3_name}
install -m 644 /home/vagrant/rpmbuild/BUILD/%{rulesfile} %{buildroot}%{rulesdestdir}/%{rulesfile}

%clean
rm -rf %{buildroot}

%post
sudo /sbin/depmod -a

%postun
/sbin/depmod -a

%files
%defattr(-,root,root)
%{destdir1}/%{object1_name}
%{destdir2}/%{object2_name}
%{destdir3}/%{object3_name}
%{rulesdestdir}/%{rulesfile}

%changelog
* Wed Jun 22 2016 European Spallation Source.
–  %{module_name} kernel module on Kernel 3.10.0-229.7.2.rt56.141.6.el7.centos.x86_64
