%define module_name mrfioc2
%define object_name mrf.ko
%define build_kernel 3.10.0-229.7.2.el7.x86_64
%define current_kernel %(uname -r)
%define destdir /usr/lib/modules/%{current_kernel}/extra/
%define rulesfile 99-mrfioc2.rules
%define rulesdestdir /etc/udev/rules.d

Summary: %{module_name} kernel module
Name: kmod-%{module_name}
Version: 1.0.0
Release: 1
License: GPL
Group: extra
BuildRoot: /usr/src/build/temp

%description
%{module_name} kernel module

%prep

%install
mkdir -p %{buildroot}/%{destdir}
mkdir -p %{buildroot}/etc/udev/rules.d/
if [ “%{build_kernel}” != “%{current_kernel}” ]; then
echo “This rpm is for %{build_kernel} kernel version. Ensure that you are using right module/kernel”
exit 1
fi
ls %{destdir} > /dev/null 2> /dev/null
if [ $? != 0 ]; then
echo “%{destdir} is not there. Unable to install the driver.”
exit 1
fi
install -m 755 /home/vagrant/rpmbuild/BUILD/%{object_name} %{buildroot}%{destdir}/%{object_name}
install -m 644 /home/vagrant/rpmbuild/BUILD/%{rulesfile} %{buildroot}%{rulesdestdir}/%{rulesfile}

%clean
rm -rf %{buildroot}

%post
/sbin/depmod -a

%postun
/sbin/depmod -a

%files
%defattr(-,root,root)
%{destdir}/%{object_name}
%{rulesdestdir}/%{rulesfile}

%changelog
* Wed Jun 22 2016 European Spallation Source.
– %{module_name} kernel module on Kernel 3.10.0-229.7.2.el7.x86_64
